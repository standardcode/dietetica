'use strict';

angular.module('base').directive('focus', function($timeout) {
	return function(scope, element) {
		$timeout(function() {
			element[0].focus();
		});
	};
});