'use strict';

angular.module('base').directive('number', function() {
	return {
		restrict: 'EA',
		transclude: true,
		replace: true,
		scope: {
			ngModel: '=',
			ngDisabled: '=',
			suffix: '@'
		},
		controller: function($scope) {
			$scope.name = _.uniqueId();
		},
		templateUrl: 'views/directives/number.html'
	};
});