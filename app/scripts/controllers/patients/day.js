'use strict';
/*jshint -W030 */

angular.module('plan').controller('DayCtrl', function($scope, $locale, $stateParams, Plan, Food) {
	$scope.open = true;
	$scope.newComponent = [];

	var patientId = $stateParams.pid;
	var day = +$stateParams.day;
	$scope.dayName = $locale.DATETIME_FORMATS.DAY[day];

	Plan.getByPatientAndDay(patientId, day).then(function(data) {
		Food.getAllByIds(_.uniq(_.pluck(data, 'product'))).then(function(products) {
			data.forEach(function(component) {
				component.product = _.find(products, {'_id': component.product});
			});
			var meals = _.groupBy(data, 'meal');
			for (var i = 0; i < 6; ++i) {
				meals[i] = meals[i] || [];
			}
			$scope.meals = meals;
		});
	});

	Food.getNames().then(function(names) {
		$scope.items = names;
	});

	$scope.content = function(item, component) {
		return (item.weight * item.product.contents[component]) / 100;
	};

	$scope.energy = function(item) {
		return (item.weight * item.product.energy) / 100;
	};

	function sum(meal, forItem) {
		return _.sum(_.map(meal, forItem));
	}

	$scope.sum = function(meal, component) {
		return sum(meal, function(item) {
			return $scope.content(item, component);
		});
	};

	$scope.sumEnergy = function(meal) {
		return sum(meal, $scope.energy);
	};

	$scope.mealWeight = function(meal) {
		return _.sum(_.map(meal, 'weight'));
	};

	$scope.remove = function(meal, item, index) {
		Plan.remove(item).then(function() {
			meal.splice(index, 1);
		});
	};

	function convert(item) {
		var entity = _.clone(item);
		entity.product = item.product._id;
		return entity;
	}

	$scope.addNewComponent = function(meal) {
		var item = _.find($scope.items, {name: $scope.newComponent[meal]});
		item && Food.get(item._id).then(function(product) {
			var component = {
				patient: patientId,
				day: day,
				meal: meal,
				type: 'component',
				product: product,
				weight: 0
			};
			Plan.post(convert(component)).then(function(response) {
				component._id = response.id;
				component._rev = response.rev;
				$scope.meals[meal].push(component);
			});
			$scope.newComponent[meal] = null;
		});
	};

	var editWeight;
	$scope.rememberWeight = function(component) {
		editWeight = component.weight;
	};

	$scope.saveWeight = function(component) {
		editWeight !== component.weight && Plan.put(convert(component)).then(function(response) {
			component._rev = response.rev;
		});
	};
});
