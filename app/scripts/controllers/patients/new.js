'use strict';

angular.module('patient').controller('PatientNewCtrl', function($scope, $stateParams, Patient) {
	$scope.$on('exit', function() {
		var patient = $scope.patient;
		if (patient) {
			patient.type = 'patient';
			Patient.post(patient).then(function(response) {
				patient._id = response.id;
				patient._rev = response.rev;
			});
		}
	});
});
