'use strict';

angular.module('patient').controller('PatientEditCtrl', function($scope, $stateParams, Patient) {
	$scope.$on('exit', function() {
		var patient = $scope.patient;
		Patient.put(patient).then(function(response) {
			patient._rev = response.rev;
		});
	});
});
