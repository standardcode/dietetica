'use strict';

angular.module('patient').controller('PatientCtrl', function($scope, $controller, $stateParams, $state, Patient, patient) {
	$controller('CommonCtrl', {$scope: $scope});
	$scope.isPatient = _.partial($state.is, 'patients.patient');
	$scope.view = true;

	Patient.decorate(patient);
	$scope.patient = patient;
	$scope.fullName = patient.firstName + ' ' + patient.lastName;

	var indicators = [{
		name: 'BMR',
		value: 10 * patient.weight + 6.25 * patient.length - 5 * patient.age + (patient.gender === 'm' ? 5 : -161), // The Mifflin St Jeor Equation,
		unit: 'kcal'
	}, {
		name: 'TMR',
		unit: 'kcal'
	}, {
		name: 'PAL',
		value: 1
	}, {
		name: 'BMI',
		value: patient.weight / Math.pow(patient.length / 100, 2),
		unit: 'kg/m2'
	}];
	indicators[1].value = indicators[0].value;
	$scope.indicators = indicators;

	$scope.remove = function() {
		Patient.remove($scope.patient).then(function() {
			$state.go('patients');
		});
	};
});
