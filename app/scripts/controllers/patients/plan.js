'use strict';

angular.module('plan').controller('PlanCtrl', function ($scope, $locale, Plan, Food) {
	$scope.days = $locale.DATETIME_FORMATS.DAY;

	$scope.foodEnergy = _.fill(0, 7);

	$scope.days.forEach(function(day, index) {
		Plan.getByPatientAndDay($scope.patient._id, index).then(function(data) {
			Food.getAllByIds(_.uniq(_.pluck(data, 'product'))).then(function(products) {
				$scope.foodEnergy[index] = _.sum(data.map(function(component) {
					return component.weight * (_.find(products, {'_id': component.product}).energy || 0) / 100;
				}));
			});
		});
	});
});
