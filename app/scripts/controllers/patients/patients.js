'use strict';

angular.module('patient').controller('PatientsCtrl', function ($scope, $controller, $state, Patient, patients) {
	$controller('CommonCtrl', {$scope: $scope});
	$scope.isPatients = _.partial($state.is, 'patients');

	patients.forEach(Patient.decorate);
	$scope.patients = patients;
});
