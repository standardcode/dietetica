'use strict';

angular.module('app').controller('StatusCtrl', function($scope, $rootScope) {
	$scope.exchange = 'off';

	function exchange(state) {
		$scope.exchange = state;
	}

	$rootScope.$on('db.sync.stop', _.partial(exchange, 'off'));
	$rootScope.$on('db.change', _.partial(exchange, 'on'));
});
