'use strict';

angular.module('food').controller('FoodCategoryCtrl', function($scope, $controller, $state, $stateParams, Food, groups) {
	$controller('FoodListCtrl', {$scope: $scope});
	$scope.elements = groups;
	$scope.category = _.find($scope.$parent.elements, {_id: $stateParams.cid});

	$scope.isCategory = _.partial($state.is, 'food.category');

	$scope.childHref = function(element) {
		return $state.href('.group', {gid: element._id});
	};

	$scope.create = function() {
		return {
			type: 'group',
			category: $scope.category._id
		};
	};

	$scope.remove = _.partial($scope.removeOne, Food.getItemsCount, 'GROUP_NOT_REMOVED_BECAUSE_NOT_EMPTY');
});
