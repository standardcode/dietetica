'use strict';

angular.module('food').controller('FoodListCtrl', function($scope, $controller, $state, Food) {
	$controller('CommonCtrl', {$scope: $scope});
	$scope.volatile = {};
	$scope.elements = [];

	$scope.validate = function(data) {
		return data.name ? true : 'FIELD_CANNOT_BE_EMPTY';
	};

	$scope.save = function(element) {
		Food.put(element).then(function(response) {
			element._rev = response.rev;
		});
	};

	$scope.addNew = function() {
		var element = $scope.create();
		element.name = $scope.volatile.newRow;
		Food.post(element).then(function(response) {
			element._id = response.id;
			element._rev = response.rev;
			$scope.elements.push(element);
			$scope.volatile.newRow = '';
		});
	};

	$scope.removeOne = function(getter, message, element) {
		getter(element._id).then(function(count) {
			if (count === 0) {
				Food.remove(element).then(function(response) {
					_.remove($scope.elements, {_id: response.id});
				});
			} else {
				$scope.$emit('message', message);
			}
		});
	};

	$scope.$on('$destroy', function() {
		$scope.$emit('message', '');
	});
});
