'use strict';

angular.module('food').controller('FoodCtrl', function($scope, $controller, $state, Food, categories) {
	$controller('FoodListCtrl', {$scope: $scope});
	$scope.elements = categories;

	$scope.isFood = _.partial($state.is, 'food');

	$scope.childHref = function(element) {
		return $state.href('.category', {cid: element._id});
	};

	$scope.create = function() {
		return {
			type: 'category'
		};
	};

	$scope.remove = _.partial($scope.removeOne, Food.getGroupsCount, 'CATEGORY_NOT_REMOVED_BECAUSE_NOT_EMPTY');

	$scope.$on('message', function(event, message) {
		$scope.message = message;
	});
});
