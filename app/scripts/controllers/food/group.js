'use strict';

angular.module('food').controller('FoodGroupCtrl', function($scope, $controller, $state, $stateParams, Food) {
	$controller('CommonCtrl', {$scope: $scope});
	$scope.group = _.find($scope.$parent.elements, {_id: $stateParams.gid});
	Food.getItems($scope.group._id).then(function(docs) {
		$scope.items = docs;
	});

	$scope.isGroup = _.partial($state.is, 'food.category.group');

	$scope.$on('$stateChangeStart', function(evt, toState, toParams, fromState) {
		if (fromState.name.indexOf(toState.name) === 0 && (Date.now() - (Food.lastGroupTime || 0)) < 100) {
			evt.preventDefault();
		}
	});

	$scope.$on('$stateChangeSuccess', function() {
		Food.lastGroupTime = Date.now();
	});
});
