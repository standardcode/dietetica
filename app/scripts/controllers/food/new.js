'use strict';

angular.module('food').controller('FoodNewItemCtrl', function($scope, $controller, $stateParams, Food) {
	$controller('CommonCtrl', {$scope: $scope});
	$scope.item = {};

	$scope.$on('exit', function() {
		var item = $scope.item;
		if(item.name) {
			item.type = 'item';
			item.category = $stateParams.cid;
			item.group = $stateParams.gid;
			Food.post(item).then(function(response) {
				item._id = response.id;
				item._rev = response.rev;
			});
		}
	});
});
