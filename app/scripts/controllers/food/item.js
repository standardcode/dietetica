'use strict';

angular.module('food').controller('FoodItemCtrl', function($scope, $controller, $state, $stateParams, Food) {
	$controller('CommonCtrl', {$scope: $scope});
	$scope.view = true;
	Food.get($stateParams.iid).then(function(doc) {
		$scope.item = doc;
		$scope.name = doc.name;
	});

	$scope.remove = function() {
		Food.remove($scope.item).then(function() {
			$state.go('^');
		});
	};

	$scope.$on('exit', function() {
		var item = $scope.item;
		Food.put(item).then(function(response) {
			item._rev = response.rev;
		});
	});
});
