'use strict';

angular.module('app').controller('CommonCtrl', function ($scope, $rootScope, $state) {
	$state.get().forEach(function(state) {
		if (!state.abstract) {
			state.onExit = function() {
				$rootScope.$broadcast('exit');
			};
		}
	});
});
