'use strict';

angular.module('app').controller('NavCtrl', function($scope) {
	$scope.menu = [{
		sref: 'home',
		label: 'HOME',
		icon: 'home'
	}, {
		sref: 'food',
		label: 'PRODUCTS',
		icon: 'cutlery'
	}, {
		sref: 'patients',
		label: 'PATIENTS',
		icon: 'users'
	}];
});
