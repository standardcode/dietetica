'use strict';

angular.module('app', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngResource', 'ui.router', 'ui.bootstrap', 'ncy-angular-breadcrumb', 'pascalprecht.translate', 'pouchdb', 'xeditable', 'base', 'food', 'patient', 'plan']).run(function(editableOptions) {
	FastClick.attach(document.body);
	editableOptions.theme = 'bs3';
}).config(function($stateProvider, $urlRouterProvider, $translateProvider, $breadcrumbProvider) {
	$breadcrumbProvider.setOptions({
		prefixStateName: 'home'
	});
	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: 'partials/main.html',
			controller: 'MainCtrl',
			data: {
				ncyBreadcrumbLabel: 'Home'
			}
		}).state('food', {
			url: '/food',
			templateUrl: 'partials/food/food.html',
			controller: 'FoodCtrl',
			data: {
				ncyBreadcrumbLabel: 'Products'
			},
			resolve: {
				categories: function(Food) {
					return Food.getAllCategories();
				}
			}
		}).state('food.category', {
			url: '/:cid',
			templateUrl: 'partials/food/category.html',
			controller: 'FoodCategoryCtrl',
			data: {
				ncyBreadcrumbLabel: '{{ category.name }}'
			},
			resolve: {
				groups: function(Food, $stateParams) {
					return Food.getGroups($stateParams.cid);
				}
			}
		}).state('food.category.group', {
			url: '/:gid',
			templateUrl: 'partials/food/group.html',
			controller: 'FoodGroupCtrl',
			data: {
				ncyBreadcrumbLabel: '{{ group.name }}'
			} // resolve doesn't work for 3rd level or further
		}).state('food.category.group.newItem', {
			url: '/new',
			controller: 'FoodNewItemCtrl',
			templateUrl: 'partials/food/item.html',
			data: {
				ncyBreadcrumbLabel: 'New item'
			}
		}).state('food.category.group.item', {
			url: '/:iid',
			controller: 'FoodItemCtrl',
			templateUrl: 'partials/food/item.html',
			data: {
				ncyBreadcrumbLabel: '{{ name }}'
			}
		}).state('patients', {
			url: '/patient',
			templateUrl: 'partials/patients/patients.html',
			controller: 'PatientsCtrl',
			data: {
				ncyBreadcrumbLabel: 'Patients'
			},
			resolve: {
				patients: function(Patient) {
					return Patient.getAll();
				}
			}
		}).state('patients.new', {
			url: '/new',
			templateUrl: 'partials/patients/new.html',
			controller: 'PatientNewCtrl',
			data: {
				ncyBreadcrumbLabel: 'New'
			}
		}).state('patients.patient', {
			url: '/:pid',
			templateUrl: 'partials/patients/patient.html',
			controller: 'PatientCtrl',
			data: {
				ncyBreadcrumbLabel: '{{ fullName }}'
			},
			resolve: {
				patient: function(Patient, $stateParams) {
					return Patient.get($stateParams.pid);
				}
			}
		}).state('patients.edit', {
			url: '/edit',
			templateUrl: 'partials/patients/edit.html',
			controller: 'PatientEditCtrl',
			data: {
				ncyBreadcrumbLabel: 'Edit'
			}
		}).state('patients.patient.day', {
			url: '/:day',
			templateUrl: 'partials/patients/day.html',
			controller: 'DayCtrl',
			data: {
				ncyBreadcrumbLabel: '{{ dayName }}'
			}
		});
	$urlRouterProvider.otherwise('/');

	$translateProvider.useStaticFilesLoader({
		prefix: 'scripts/i18n/',
		suffix: '.json'
	});
	$translateProvider.preferredLanguage('en');
});
