'use strict';

_.mixin({
	sum: function(array) {
		return array.reduce(function(a, b) {
			return (a || 0) + (b || 0);
		}, 0);
	},
	fill: function(value, length) {
		return Array.apply(null, new Array(length)).map(_.constant(value));
	},
	sortByString: ''.localeCompare ?
		function(array, key) {
			array = _.clone(array) || [];
			array.sort(function(a, b) {
				a = a && a[key] || '';
				b = b && b[key] || '';
				return a.localeCompare(b, {usage: 'sort', sensitivity: 'accent'});
			});
			return array;
		} : _.sortBy
});
