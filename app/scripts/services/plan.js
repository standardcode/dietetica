'use strict';

angular.module('plan', []).service('Plan', function Plan($locale, DB) {
	_.assign(this, DB);

	function map(doc) {
		if (doc.type) {
			switch (doc.type) {
				case 'component':
					emit([doc.patient, doc.day]);
					break;
			}
		}
	}

	this.getByPatientAndDay = function(patient, day) {
		return DB.getWhere(map, [patient, day]);
	};
});
