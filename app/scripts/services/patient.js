'use strict';

angular.module('patient', []).service('Patient', function Patient($q, DB) {
	_.assign(this, DB);

	function map(doc) {
		if (doc.type) {
			switch (doc.type) {
				case 'patient':
					emit([doc._id]);
					break;
			}
		}
	}

	this.getAll = _.partial(DB.getAll, map);

	this.decorate = function (doc) {
		Object.defineProperties(doc, {
			age: {
				get: function () {
					return Math.abs(new Date(Date.now() - this.birthday).getUTCFullYear() - 1970);
				}
			}
		});
	};
});
