'use strict';
/*jshint -W030 */

angular.module('food', []).service('Food', function Food($q, DB) {
	_.assign(this, DB);

	function itemsMap(doc) {
		doc.type === 'item' && emit(doc.group);
	}

	this.getAllCategories = _.partial(DB.getAll, function(doc) {
		doc.type === 'category' && emit(doc._id);
	});

	function groupsMap(doc) {
		doc.type === 'group' && emit(doc.category);
	}

	this.getGroups = _.partial(DB.getWhere, groupsMap);
	this.getGroupsCount = _.partial(DB.getCount, groupsMap);

	this.getItems = _.partial(DB.getWhere, itemsMap);
	this.getItemsCount = _.partial(DB.getCount, itemsMap);

	this.getAll = _.partial(DB.getAll, itemsMap);

	this.getNames = function() {
		var deferred = $q.defer();
		this.getAll().then(function(docs) {
			var names = _.sortBy(_.filter(_.filter(docs, {type: 'item'}), 'name').map(function(item) {
				return {
					_id: item._id,
					name: item.name
				};
			}), 'name');
			deferred.resolve(names);
		}).catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	};
});
