'use strict';
/*jshint camelcase: false */

angular.module('base', []).service('DB', function DB(pouchdb, $q, $rootScope, COUCHDB_URL) {
	var db = pouchdb.create('food');

	var finished = _.debounce(function() {
		$rootScope.$apply(function() {
			$rootScope.$emit('db.sync.stop');
		});
	}, 600);

	var change = _.debounce(function() {
		$rootScope.$apply(function() {
			$rootScope.$emit('db.change');
		});
	}, 100);

	db.replicate.sync(COUCHDB_URL + '/food', {
		live: true
	}).on('change', change).on('uptodate', finished).on('error', function(error) {
		console.error(error);
		finished();
	});

	function convert(transform, query) {
		var deferred = $q.defer();
		query.then(function(response) {
			deferred.resolve(transform(response));
		}).catch(function(error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	var wrap = _.partial(convert, function(response) {
		return _.map(response.rows, 'doc');
	});

	var count = _.partial(convert, function(response) {
		return response.total_rows;
	});

	this.get = db.get;

	this.getAll = function(map) {
		return wrap(db.query(map, {include_docs: true}));
	};

	this.getAllByIds = function(ids) {
		return wrap(db.allDocs({keys: ids, include_docs: true}));
	};

	this.getWhere = function(map, id) {
		return wrap(db.query(map, {key: id, include_docs: true}));
	};

	this.getCount = function(map, id) {
		return count(db.query(map, {key: id, include_docs: false, reduce: '_count'}));
	};

	this.put = db.put;

	this.post = db.post;

	this.remove = db.remove;
});
