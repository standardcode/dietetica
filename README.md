# Dietetica

This is a web application for creating diet plans.
Still in development, not ready to use!

## Parts

* Products database, with options to add, edit or view details of product
* Patients database, with options to edit patient details and creating weekly diet plan

## Features

* Works in modern browsers, both desktop and mobile
* Works offline
* Remote database used for synchronization

## Technical details

* AngularJS - framework
* PouchDB - in-browser database
* CouchDB - remote database
* Bower, Gulp - build tools
* Bootstrap - Sass framework